#include <memory>
#include <iostream>
#include <chrono>
#include <vector>
#include <string>

struct testStruct {
    int x;
    int compute() const { return 2 + 4; }
    testStruct() { memset(this, 0, sizeof(testStruct)); }
};

void computeConstRef(const std::shared_ptr<testStruct>& ptr) {
    ptr->compute();
    ptr->x = 4;
}

void computeConstPtr(const std::shared_ptr<testStruct> ptr) {
    ptr->compute();
    ptr->x = 4;
}

void computePtr(std::shared_ptr<testStruct> ptr) {
    ptr->compute();
    ptr->x = 4;
}

void computeptrConstObject(std::shared_ptr<const testStruct> ptr) {
    ptr->compute();
    //ptr->x = 4;  does not work cause const!
}

void computeptrRef(std::shared_ptr<testStruct> &ptr) {
    ptr->compute();
    ptr->x = 4;
}

int main()
{
    uint32_t containerSize = 25000;
    std::cout << "Starting test...!\n";

    auto ptrvector = std::vector<std::shared_ptr<testStruct>>(containerSize);

    auto ptrvector2 = std::vector<std::shared_ptr<testStruct>>(containerSize);


    auto start = std::chrono::high_resolution_clock::now();

    for (auto& r : ptrvector) {
        r = std::make_shared<testStruct>();
        computeConstRef(r);
    }


    auto end = std::chrono::high_resolution_clock::now();
    auto duration = (end - start);

    auto durationInMs = std::chrono::duration_cast<std::chrono::microseconds>(duration);

    std::string durationStr = "Pass by const ref time: " + std::to_string(durationInMs.count()) + " microseconds";

    std::cout << durationStr << std::endl;

    // *****************************************************************************
    auto start2 = std::chrono::high_resolution_clock::now();

    for (auto& r : ptrvector2) {
        r = std::make_shared<testStruct>();
        computePtr(r);
    }

    auto end2 = std::chrono::high_resolution_clock::now();
    auto duration2 = (end2 - start2);

    auto durationInMs2 = std::chrono::duration_cast<std::chrono::microseconds>(duration);

    auto durationStr2 = "Pass by ptr: " + std::to_string(durationInMs.count()) + " microseconds";

    std::cout << durationStr2 << std::endl;
}
